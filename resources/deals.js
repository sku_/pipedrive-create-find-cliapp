//const { fields } = require("../authentication");

const _sharedBaseUrl='https://api.pipedrive.com/v1/';

let param = (z, bundle) => {
  let dict={};
  dict['term']=bundle.inputData.term;
  if (bundle.inputData.fields && bundle.inputData.fields.trim().length > 0){
    dict['fields']=bundle.inputData.fields;
  }
  if (bundle.inputData.exact_match && bundle.inputData.exact_match.trim().length > 0){
    dict['exact_match']=bundle.inputData.exact_match;
  }if (bundle.inputData.person_id && bundle.inputData.person_id.trim().length > 0){
    dict['person_id']=bundle.inputData.person_id;
  }if (bundle.inputData.organization_id && bundle.inputData.organization_id.trim().length > 0){
    dict['organization_id']=bundle.inputData.organization_id;
  }if (bundle.inputData.status && bundle.inputData.status.trim().length > 0){
    dict['status']=bundle.inputData.status;
  }if (bundle.inputData.include_fields && bundle.inputData.include_fields.trim().length > 0){
    dict['include_fields']=bundle.inputData.include_fields;
  }if (bundle.inputData.start && bundle.inputData.start.trim().length > 0){
    dict['start']=bundle.inputData.start;
  }if (bundle.inputData.limit && bundle.inputData.limit.trim().length > 0){
    dict['limit']=bundle.inputData.limit;
  }
  z.console.log(dict);
  return dict;
}
const searchDeal = (z, bundle) => {
  return z.request({
    url: _sharedBaseUrl+'deals/search',
    params: param(z,bundle)
    // {
    //   term: bundle.inputData.term,
    //  fields: bundle.inputData.fields,
    //  exact_match: bundle.inputData.exact_match,
    //  person_id: bundle.inputData.person_id,
    //  organization_id: bundle.inputData.organization_id,
    //  status: bundle.inputData.status,
    //  include_fields: bundle.inputData.include_fields,
    //  start: bundle.inputData.start,
    //  limit: bundle.inputData.limit
    // }
  })
  .then(function(response){
    let arr = [];
    arr.push(response.data);
    return arr;
    //return response.data.data.items;
    // arr[0]=(response.data);
    // z.console.log("@@",arr);
    // return arr;
  });
  //.then((response)=> this.arr.push(response))
  //.then((response)=> this.arr);
//   return [{
//     "result_score": 0.8766,
//     "item": {
//         "id": 163,
//         "type": "deal",
//         "title": "Lone - Varebiliventar.dk",
//         "value": null,
//         "currency": "DKK",
//         "status": "open",
//         "visible_to": 7,
//         "owner": {
//             "id": 1968407
//         },
//         "stage": {
//             "id": 90,
//             "name": "Qualified"
//         },
//         "person": {
//             "id": 127,
//             "name": "Lone Porsgaard Rasmussen"
//         },
//         "organization": {
//             "id": 31,
//             "name": "Dansk Varebilinventar",
//             "address": null
//         },
//         "custom_fields": [],
//         "notes": []
//     }
// }]
};
//let arr = [];
const createDeal = (z, bundle) => {
  const requestOptions = {
    url: _sharedBaseUrl + 'deals',
    method: 'POST',
    body: JSON.stringify({
      title: bundle.inputData.title,
      value: bundle.inputData.value,
      currency: bundle.inputData.currency,
      user_id: bundle.inputData.user_id,
      person_id: bundle.inputData.person_id,
      org_id: bundle.inputData.org_id,
      stage_id: bundle.inputData.stage_id,
      status: bundle.inputData.status,
      expected_close_date: bundle.inputData.expected_close_date,
      probability: bundle.inputData.probability,
      lost_reason: bundle.inputData.lost_reason,
      visible_to: bundle.inputData.visible_to,
      add_time: bundle.inputData.add_time
    }),
    headers: {
      'content-type': 'application/json'
    }
  };
  
  return z.request(requestOptions)
  .then((response) => JSON.parse(response.content));
};

const sample1 = {
  term: 'searchable term',
  fields: 'abc',
  exact_match: true,
  person_id: 1,
  organization_id: 2,
  status: 'Open',
  include_fields: '',
  start: null,
  limit: 1
};

const sample2 = {
  title: 1,
  value: 'abc',
  currency: 'USD',
  user_id: 1,
  person_id: 2,
  org_id: 3,
  stage_id: 2,
  status: 'Open',
  expected_close_date: '2021-04-23',
  probability: 80,
  lost_reason: 'abc',
  visible_to: 'abc',
  add_time: '2020-02-23 12:12:12'
};

module.exports = {
  key: 'deals',
  noun: 'Deals',
  search: {
    display: {
      label: 'Search Deal',
      description: 'finds a deal.',
    },
    operation: {
      inputFields: [
        {key: 'term', required: true, type: 'string'},
        {key: 'fields', required: false, type: 'string'},
        {key: 'exact_match', required: false, type: 'boolean'},
        {key: 'person_id', required: false, type: 'integer'},
        {key: 'organization_id', required: false, type: 'integer'},
        {key: 'status', required: false, type: 'string'},
        {key: 'include_fields', required: false, type: 'string'},
        {key: 'start', required: false, type: 'integer'},
        {key: 'limit', required: false, type: 'integer'}
      ],
      perform: searchDeal,
      sample: sample1
    },
  },

  create: {
    display: {
      label: 'Create deal',
      description: 'Adds a new deal.',
    },
    operation: {
      inputFields: [
        {key: 'title', required: true, type: 'string'},
        {key: 'value', required: false, type: 'string', helpText: 'Value of the deal. If omitted, value will be set to 0.'},
        {key: 'currency', required: false, type: 'string', label: 'Accepts 3-character currency'},
        {key: 'user_id', required: false, type: 'integer', helpText: 'Owner of deal'},
        {key: 'person_id', required: false, type: 'integer', helpText: 'person this deal will be associated'},
        {key: 'org_id', required: false, type: 'integer', helpText: 'Organisation associated with deal'},
        {key: 'stage_id', required: false, type: 'integer', helpText: 'stage, this deal will be placed in a pipeline'},
        {key: 'status', required: false, type: 'string', helpText: 'open = Open , won = Won , lost = Lost , deleted = Deleted. If omitted , status will be open.'},
        {key: 'expected_close_date', required: false, type: 'string', helpText: 'In ISO 8601 format: YYYY-MM-DD'},
        {key: 'probability', required: false, type: 'integer', helpText: 'Deal sucess percentage'},
        {key: 'lost_reason', required: false, type: 'string', helpText: 'Why deal was lost'},
        {key: 'visible_to', required: false, type: 'string', helpText: 'Visibility of the deal'},
        {key: 'add_time', required: false, type: 'string', helpText: 'Creation date & time of the deal in UTC. Format YYYY-MM-DD HH:MM:SS'}
      ],
      perform: createDeal,
      sample: sample2
    },
  },
 // outputFields: () => { return []; }
}