const Deals  = require('./resources/deals');
const authentication = require('./authentication');

//const addAuthHeader = (request, z, bundle) => {
  // Hard-coded authentication just for demo
//  request.params['api_token'] = 'api key';
//  return request;
//};

const includeApiKey = (request, z, bundle) => {
  if (bundle.authData.api_key) {
    request.params = request.params || {};
    request.params.api_token = bundle.authData.api_key;
    //z.console.log(request.params);
    //
    // request.headers.Authorization = bundle.authData.apiKey;
    // (If you want to include the key as a header instead)
    //
  }
  return request;
};
 
module.exports = {
  // This is just shorthand to reference the installed dependencies you have.
  // Zapier will need to know these before we can upload.
  version: require('./package.json').version,
  platformVersion: require('zapier-platform-core').version,

  authentication: authentication,

  beforeRequest: [includeApiKey],

  afterResponse: [],

  resources: {
    [Deals.key]: Deals
  },

  // If you want your trigger to show up, you better include it here!
  triggers: {},

  // If you want your searches to show up, you better include it here!
  searches: {},

  // If you want your creates to show up, you better include it here!
  creates: {},
};